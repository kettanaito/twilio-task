export default function debounce(fn, threshold) {
  let timeout

  return function () {
    const funcCall = () => fn.apply(this, arguments)
    clearTimeout(timeout)
    timeout = setTimeout(funcCall, threshold)
  }
}
