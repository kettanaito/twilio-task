import React, { Component } from 'react';
import UserStatus from './components/UserStatus'

class App extends Component {
  render() {
    return (
      <UserStatus
        statusColor="#00BB48"
        imageUrl="https://avatars0.githubusercontent.com/u/14984911?s=460&v=4"
        primaryText="Artem Zakharchenko"
        secondaryText="Online | 4h"
      />
    );
  }
}

export default App;
