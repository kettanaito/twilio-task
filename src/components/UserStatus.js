import React from 'react'
import PropTypes from 'prop-types'
import debounce from '../utils/debounce'
import './UserStatus.css'

class UserStatus extends React.Component {
  static propTypes = {
    statusColor: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    primaryText: PropTypes.string.isRequired,
    secondaryText: PropTypes.string.isRequired,
    breakpointWidth: PropTypes.number.isRequired,
  }

  static defaultProps = {
    breakpointWidth: 160,
  }

  state = {
    isMobile: false,
  }

  componentDidMount() {
    this.redraw()
    this.resizeHandler = debounce(this.redraw, 100)
    window.addEventListener('resize', this.resizeHandler)
  }

  redraw = () => {
    const { breakpointWidth } = this.props

    this.setState({
      isMobile: this.container.clientWidth <= breakpointWidth,
    })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeHandler)
  }

  render() {
    const { isMobile } = this.state
    const { statusColor, imageUrl, primaryText, secondaryText } = this.props

    const containerClassNames = [
      'user-status',
      isMobile && 'user-status--compact'
    ].filter(Boolean).join(' ')
    
    return (
      <div
        ref={container => this.container = container}
        className={containerClassNames}>
        <div className="user-status__meta">
          <img
            className="user-status__image"
            src={imageUrl}
            alt={primaryText}
          />
          <div
            className="user-status__pin"
            style={{ backgroundColor: statusColor }} />
        </div>
        <div className="user-status__content">
          <p className="user-status__primary text--overflow">{primaryText}</p>
          <p className="user-status__secondary text--overflow">{secondaryText}</p>
        </div>
      </div>
    )
  }
}

export default UserStatus
